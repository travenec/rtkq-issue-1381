import { IModule } from "redux-dynamic-modules";
import { api as petstoreApi } from "./petstore-api.generated";

export const getCoreReduxModule = (): IModule<any> => {
  return {
    id: "core",
    reducerMap: {
      [petstoreApi.reducerPath]: petstoreApi.reducer
    },
    middlewares: [petstoreApi.middleware]
  };
};