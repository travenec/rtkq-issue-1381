import { createStore, IModuleStore } from "redux-dynamic-modules";
import { getThunkExtension } from "redux-dynamic-modules-thunk";
import { getCoreReduxModule } from "./coreModule";

/**
 * Dynamic module definition for redux-dynamic-modules
 * The "core" module - initial module
 * and thunk extension
 */
export const store: IModuleStore<any> = createStore({
  extensions: [getThunkExtension()]
}, getCoreReduxModule());