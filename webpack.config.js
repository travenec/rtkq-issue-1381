const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: "./src/index.tsx",
  mode: "development",
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  devServer: {
    contentBase: "./dist",
    historyApiFallback: true
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  output: {
    path: path.resolve(__dirname, "dist")
  },
  plugins: [
    // vytvari index.html podle zadanych parametru
    new HtmlWebpackPlugin({
      // hash: true,
      title: "RTKQ",
      template: "./src/index.ejs",
      filename: "./index.html",
      publicPath: "/"
    })
  ]
};